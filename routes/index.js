const express = require('express');
const jwt = require('atlassian-jwt');
const { to } = require('await-to-js');
const { Client } = require('../models');

const attlasianConnect = require('../atlassian-connect.json');
const { getAllIssues } = require('../services/restApi');

const router = express.Router();

// return json config for attlasian
router.get('/', async (req, res) => {
    res.send(attlasianConnect);
});

// save neсessary info in mongo
router.post('/installed', async (req, res) => {
    const newClient = {
        clientKey: req.body.clientKey,
        sharedSecret: req.body.sharedSecret,
        baseUrl: req.body.baseUrl,
    };

    const [err, client] = await to(Client.create(newClient));
    if (err) {
        console.error(err);
        res.status(500).send();
    }

    res.status(204).send();
});

// return issues
router.get('/issues', async (req, res) => {
    let err, user, response;

    // decode jwt from atlassian request
    const payload = await jwt.decode(req.query.jwt, null, true);

    // find info by clientKey
    [err, user] = await to(Client.findOne({ clientKey: payload.iss }));
    if (err) {
        console.error(err);
        res.status(500).send();
    }

    // get issues by request to atllasian
    [err, response] = await to(getAllIssues(user.baseUrl, req.query.projectKey, user.sharedSecret));
    if (err) {
        console.error(err);
        res.status(500).send();
    }

    res.render('index', { issues: response.issues.map(issue => issue.fields.summary) });
});

module.exports = router;
