const mongoose = require('mongoose');

const Client = require('./client');

require('dotenv').config();

mongoose.Promise = global.Promise;

const models = { Client };

mongoose.connect(process.env.DB_LOCATION).catch((err) => {
    console.error('Can Not Connect to Mongo Server');
});

const db = mongoose.connection;

db.once('open', () => {
    console.log('Connected to mongo');
});

db.on('error', (error) => {
    console.error('error', error);
});

module.exports = models;
