const mongoose = require('mongoose');

const { Schema } = mongoose;

const ClientSchema = Schema({
    clientKey: {
        type: String,
        index: true,
        unique: true,
    },
    sharedSecret: {
        type: String,
        required: true,
    },
    baseUrl: {
        type: String,
        required: true,
    },
}, {
    timestamps: false,
    versionKey: false,
});

module.exports = mongoose.model('Client', ClientSchema);
