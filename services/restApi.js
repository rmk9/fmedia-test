const axios = require('axios');
const { generateToken } = require('./token');

module.exports = {
    getAllIssues: async (baseUrl, projectKey, secret) => {
        const method = 'GET';
        const url = `/rest/api/3/search?jql=project=${projectKey}`;

        const token = await generateToken(method, url, secret);

        const result = await axios({
            url: `${baseUrl}${url}`,
            method: 'get',
            headers: { Authorization: `JWT ${token}` },
        });

        return result.data;
    },
};
