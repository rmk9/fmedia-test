const jwt = require('atlassian-jwt');
const moment = require('moment');

module.exports = {
    generateToken: async (method, url, secret) => {
        const now = moment().utc();

        const req = jwt.fromMethodAndUrl(method, url);

        const tokenData = {
            iss: process.env.PLUGIN_KEY,
            iat: now.unix(),
            exp: now.add(90, 'minutes').unix(),
            qsh: jwt.createQueryStringHash(req),
        };

        return jwt.encode(tokenData, secret);
    },
};
