const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const morgan = require('morgan');

const routes = require('./routes');

require('dotenv').config();

const app = express();

// logger
app.use(morgan('dev'));

// response
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// view
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// routes
app.use('/', routes);

app.all('*', (req, res) => {
    res.redirect('/');
});

const port = process.env.PORT;
app.listen(port, () => {
    console.log('server started - ', port);
});
